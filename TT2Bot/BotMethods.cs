﻿
using AutoItX3Lib;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace TT2Bot
{
    public class BotMethods
    {
        public AutoItX3 autoit = new AutoItX3();
        public writerDelegate writer;
        public delegate void writerDelegate(String new_item);
        public bool updateFormText;





        /// <summary>
        /// METHODS:
        /// *Running:
        ///     - Prestige
        /// *Ideas:
        ///     - AutoClic
        ///     - Add watcher
        ///     - FUTURE Ocr
        /// </summary>

        public BotMethods()
        {
            //writer = new writerDelegate(WriteLine);
            this.updateFormText = true;
        }

        //public void WriteLine(string message)
        //{
        //    try
        //    {
        //        form.Invoke(writer, new string[]
        //        {
        //            message
        //        });
        //        using (StreamWriter w = File.AppendText($"TT2Bot-Log.txt"))
        //        {
        //            w.WriteLine(message);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}
        public void posicionarBlue()
        {
            //autoit.WinMove("BlueStacks", "", 1314, 398);
            autoit.WinActivate("NoxPlayer");
            autoit.WinMove("NoxPlayer", "", 1301, 397);
            autoit.WinActivate("NoxPlayer");
        }
        public Process StartBlueStacks()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();

            startInfo.FileName = @"C:\Program Files\BlueStacks\HD-RunApp.exe";
            startInfo.Arguments = @"-p com.gamehivecorp.taptitans2";
            var aa = Process.Start(startInfo);
            return aa;
            //aa.WaitForInputIdle();
        }
         public void CheckWindow()
        {
            if (autoit.WinExists("NoxPlayer") == 0)
            {
                System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();
                proc.FileName = @"C:\Program Files(x86)\BlueStacks\HD - RunApp.exe";
                proc.Arguments = @"-p com.gamehivecorp.taptitans2";
                System.Diagnostics.Process.Start(proc);
                autoit.WinActivate("BlueStacks");   //visualizar ventana

            }
            autoit.WinActivate("BlueStacks");   //visualizar ventana
         }
         //public void WriteLines(string message)
         //{
         //   int num = 0;
         //   int num2 = 0;
         //   string[] lines = form.richTextBox1.Lines;
         //   string[] array = lines;
         //   for (int i = 0; i < array.Length; i++)
         //   {
         //       string text = array[i];
         //       if (num >= 2)
         //       {
         //           lines[num - 1] = text;
         //       }
         //       num++;
         //   }
         //   if (lines.Length > 1)
         //   {
         //       num2 = lines.Length - 2;
         //   }
         //   message = "[" + DateTime.Now.ToString("T") + "] - " + message;
         //   lines[num2] = message;
         //   form.richTextBox1.Lines = lines;
         //   form.richTextBox1.SelectionStart = form.richTextBox1.Text.Length;
         //   form.richTextBox1.ScrollToCaret();
         //}
        public void MakePrestige()
        {
            autoit.WinActivate("NoxPlayer");
            autoit.MouseClick("left", 1330, 1024, 1, -1);
            autoit.MouseClick("left", 1574, 765, 1, -1);

            autoit.MouseClickDrag("left", 1394, 977, 1364, 355, -1);
            autoit.MouseClick("left", 1578, 987, 1, -1);

        }

        public void autoClick()
        {
            autoit.WinActivate("NoxPlayer");
            autoit.MouseClick("left", 1463, 707, 5, -1);
        }
        public void upgradeLevel()
        {
            //autoit.WinActivate("NoxPlayer");
            //autoit.MouseClick("left", 1330, 1024, 1, -1);
            //autoit.MouseClick("left", 1574, 765, 1, -1);
            int[] cords = new int[2];
            cords[1] = int.Parse(autoit.PixelSearch(1537, 502, 1638, 1010, 0xF38914, 0, 1));
            //dynamic pix = autoit.PixelSearch(1537, 502, 1638, 1010, 0xF38914, 3);
            //autoit.PixelGetColor()
            autoit.MouseClick("left", 1463, 707, 5, -1);
        }
        public void StopBot()
        {
            Form1.self.timerWindowScreenShot.Stop();
            Form1.self.timerAttack.Stop();
            Form1.self.timerFairy.Stop();
            Form1.self.timerPJLevel.Stop();
            Form1.self.timerPositionMana.Stop();
            Form1.self.timerPrestige.Stop();
            Form1.self.timerTime.Stop();
            try
            {
                foreach (Process proc in Process.GetProcessesByName("AutoIt"))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

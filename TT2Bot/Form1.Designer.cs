﻿namespace TT2Bot
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxAbTime = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxAttack = new System.Windows.Forms.TextBox();
            this.checkBoxAttack = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxPJLevel = new System.Windows.Forms.TextBox();
            this.textBoxPrestige = new System.Windows.Forms.TextBox();
            this.checkBoxPJLev = new System.Windows.Forms.CheckBox();
            this.checkBoxPrestige = new System.Windows.Forms.CheckBox();
            this.labelMana = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelMoney = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ScreenShotTime = new System.Windows.Forms.DomainUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPrestiges = new System.Windows.Forms.Label();
            this.checkBoxWarCry = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.labelRuntime = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelStages = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelRelics = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelDiamonds = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxHandMidas = new System.Windows.Forms.CheckBox();
            this.checkBoxDeadlyStrike = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.VIP_Checkbox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.timerWindowScreenShot = new System.Windows.Forms.Timer(this.components);
            this.timerFairy = new System.Windows.Forms.Timer(this.components);
            this.timerPositionMana = new System.Windows.Forms.Timer(this.components);
            this.timerPrestige = new System.Windows.Forms.Timer(this.components);
            this.timerPJLevel = new System.Windows.Forms.Timer(this.components);
            this.timerAttack = new System.Windows.Forms.Timer(this.components);
            this.timerTime = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timerAbs = new System.Windows.Forms.Timer(this.components);
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-3, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(439, 484);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.textBoxAbTime);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.textBoxAttack);
            this.tabPage1.Controls.Add(this.checkBoxAttack);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.textBoxPJLevel);
            this.tabPage1.Controls.Add(this.textBoxPrestige);
            this.tabPage1.Controls.Add(this.checkBoxPJLev);
            this.tabPage1.Controls.Add(this.checkBoxPrestige);
            this.tabPage1.Controls.Add(this.labelMana);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.labelMoney);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.ScreenShotTime);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.labelPrestiges);
            this.tabPage1.Controls.Add(this.checkBoxWarCry);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.labelRuntime);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.labelStages);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.labelRelics);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.labelDiamonds);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.checkBoxHandMidas);
            this.tabPage1.Controls.Add(this.checkBoxDeadlyStrike);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(431, 458);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Options";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(282, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "secs";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(176, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 46;
            this.label16.Text = "Use every:";
            // 
            // textBoxAbTime
            // 
            this.textBoxAbTime.Location = new System.Drawing.Point(240, 18);
            this.textBoxAbTime.Name = "textBoxAbTime";
            this.textBoxAbTime.Size = new System.Drawing.Size(36, 20);
            this.textBoxAbTime.TabIndex = 45;
            this.textBoxAbTime.Text = "90";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(355, 103);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 13);
            this.label15.TabIndex = 43;
            this.label15.Text = "secs";
            // 
            // textBoxAttack
            // 
            this.textBoxAttack.Location = new System.Drawing.Point(313, 99);
            this.textBoxAttack.Name = "textBoxAttack";
            this.textBoxAttack.Size = new System.Drawing.Size(36, 20);
            this.textBoxAttack.TabIndex = 42;
            this.textBoxAttack.Text = "3";
            // 
            // checkBoxAttack
            // 
            this.checkBoxAttack.AutoSize = true;
            this.checkBoxAttack.Location = new System.Drawing.Point(217, 102);
            this.checkBoxAttack.Name = "checkBoxAttack";
            this.checkBoxAttack.Size = new System.Drawing.Size(89, 17);
            this.checkBoxAttack.TabIndex = 41;
            this.checkBoxAttack.Text = "Attack every:";
            this.checkBoxAttack.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(155, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 40;
            this.label14.Text = "min";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(155, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "min";
            // 
            // textBoxPJLevel
            // 
            this.textBoxPJLevel.Location = new System.Drawing.Point(113, 122);
            this.textBoxPJLevel.Name = "textBoxPJLevel";
            this.textBoxPJLevel.Size = new System.Drawing.Size(36, 20);
            this.textBoxPJLevel.TabIndex = 38;
            this.textBoxPJLevel.Text = "1";
            // 
            // textBoxPrestige
            // 
            this.textBoxPrestige.Location = new System.Drawing.Point(113, 100);
            this.textBoxPrestige.Name = "textBoxPrestige";
            this.textBoxPrestige.Size = new System.Drawing.Size(36, 20);
            this.textBoxPrestige.TabIndex = 37;
            this.textBoxPrestige.Text = "50";
            // 
            // checkBoxPJLev
            // 
            this.checkBoxPJLev.AutoSize = true;
            this.checkBoxPJLev.Location = new System.Drawing.Point(17, 125);
            this.checkBoxPJLev.Name = "checkBoxPJLev";
            this.checkBoxPJLev.Size = new System.Drawing.Size(92, 17);
            this.checkBoxPJLev.TabIndex = 36;
            this.checkBoxPJLev.Text = "PJ level every";
            this.checkBoxPJLev.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrestige
            // 
            this.checkBoxPrestige.AutoSize = true;
            this.checkBoxPrestige.Location = new System.Drawing.Point(17, 102);
            this.checkBoxPrestige.Name = "checkBoxPrestige";
            this.checkBoxPrestige.Size = new System.Drawing.Size(93, 17);
            this.checkBoxPrestige.TabIndex = 34;
            this.checkBoxPrestige.Text = "Prestige every";
            this.checkBoxPrestige.UseVisualStyleBackColor = true;
            // 
            // labelMana
            // 
            this.labelMana.AutoSize = true;
            this.labelMana.Enabled = false;
            this.labelMana.Location = new System.Drawing.Point(365, 386);
            this.labelMana.Name = "labelMana";
            this.labelMana.Size = new System.Drawing.Size(13, 13);
            this.labelMana.TabIndex = 33;
            this.labelMana.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Enabled = false;
            this.label13.Location = new System.Drawing.Point(256, 386);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Got Money:";
            // 
            // labelMoney
            // 
            this.labelMoney.AutoSize = true;
            this.labelMoney.Enabled = false;
            this.labelMoney.Location = new System.Drawing.Point(365, 363);
            this.labelMoney.Name = "labelMoney";
            this.labelMoney.Size = new System.Drawing.Size(49, 13);
            this.labelMoney.TabIndex = 31;
            this.labelMoney.Text = "0000000";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Location = new System.Drawing.Point(256, 363);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Got Money:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(164, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "/s";
            // 
            // ScreenShotTime
            // 
            this.ScreenShotTime.Enabled = false;
            this.ScreenShotTime.Location = new System.Drawing.Point(115, 75);
            this.ScreenShotTime.Name = "ScreenShotTime";
            this.ScreenShotTime.Size = new System.Drawing.Size(48, 20);
            this.ScreenShotTime.TabIndex = 28;
            this.ScreenShotTime.Text = "5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(14, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "ScreenShot Time:";
            // 
            // labelPrestiges
            // 
            this.labelPrestiges.AutoSize = true;
            this.labelPrestiges.Enabled = false;
            this.labelPrestiges.Location = new System.Drawing.Point(138, 431);
            this.labelPrestiges.Name = "labelPrestiges";
            this.labelPrestiges.Size = new System.Drawing.Size(13, 13);
            this.labelPrestiges.TabIndex = 26;
            this.labelPrestiges.Text = "0";
            // 
            // checkBoxWarCry
            // 
            this.checkBoxWarCry.AutoSize = true;
            this.checkBoxWarCry.Location = new System.Drawing.Point(228, 47);
            this.checkBoxWarCry.Name = "checkBoxWarCry";
            this.checkBoxWarCry.Size = new System.Drawing.Size(64, 17);
            this.checkBoxWarCry.TabIndex = 8;
            this.checkBoxWarCry.Text = "War Cry";
            this.checkBoxWarCry.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Location = new System.Drawing.Point(29, 431);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Made Prestiges:";
            // 
            // labelRuntime
            // 
            this.labelRuntime.AutoSize = true;
            this.labelRuntime.Location = new System.Drawing.Point(345, 3);
            this.labelRuntime.Name = "labelRuntime";
            this.labelRuntime.Size = new System.Drawing.Size(13, 13);
            this.labelRuntime.TabIndex = 24;
            this.labelRuntime.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(212, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Runtime(HH:Mm:Ss):";
            // 
            // labelStages
            // 
            this.labelStages.AutoSize = true;
            this.labelStages.Enabled = false;
            this.labelStages.Location = new System.Drawing.Point(138, 409);
            this.labelStages.Name = "labelStages";
            this.labelStages.Size = new System.Drawing.Size(13, 13);
            this.labelStages.TabIndex = 22;
            this.labelStages.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Location = new System.Drawing.Point(29, 409);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Improved Stages:";
            // 
            // labelRelics
            // 
            this.labelRelics.AutoSize = true;
            this.labelRelics.Enabled = false;
            this.labelRelics.Location = new System.Drawing.Point(138, 386);
            this.labelRelics.Name = "labelRelics";
            this.labelRelics.Size = new System.Drawing.Size(13, 13);
            this.labelRelics.TabIndex = 20;
            this.labelRelics.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(29, 386);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Got Relics:";
            // 
            // labelDiamonds
            // 
            this.labelDiamonds.AutoSize = true;
            this.labelDiamonds.Enabled = false;
            this.labelDiamonds.Location = new System.Drawing.Point(138, 363);
            this.labelDiamonds.Name = "labelDiamonds";
            this.labelDiamonds.Size = new System.Drawing.Size(13, 13);
            this.labelDiamonds.TabIndex = 18;
            this.labelDiamonds.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(29, 363);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Got Diamonds:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(14, 333);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Stats:";
            // 
            // checkBoxHandMidas
            // 
            this.checkBoxHandMidas.AutoSize = true;
            this.checkBoxHandMidas.Location = new System.Drawing.Point(127, 47);
            this.checkBoxHandMidas.Name = "checkBoxHandMidas";
            this.checkBoxHandMidas.Size = new System.Drawing.Size(95, 17);
            this.checkBoxHandMidas.TabIndex = 7;
            this.checkBoxHandMidas.Text = "Hand of Midas";
            this.checkBoxHandMidas.UseVisualStyleBackColor = true;
            // 
            // checkBoxDeadlyStrike
            // 
            this.checkBoxDeadlyStrike.AutoSize = true;
            this.checkBoxDeadlyStrike.Location = new System.Drawing.Point(32, 47);
            this.checkBoxDeadlyStrike.Name = "checkBoxDeadlyStrike";
            this.checkBoxDeadlyStrike.Size = new System.Drawing.Size(89, 17);
            this.checkBoxDeadlyStrike.TabIndex = 6;
            this.checkBoxDeadlyStrike.Text = "Deadly Strike";
            this.checkBoxDeadlyStrike.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Abilities (when its possible):";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.radioButton2);
            this.tabPage2.Controls.Add(this.radioButton1);
            this.tabPage2.Controls.Add(this.checkBox9);
            this.tabPage2.Controls.Add(this.VIP_Checkbox);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(431, 458);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(7, 274);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(421, 39);
            this.button1.TabIndex = 7;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(97, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(38, 17);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "2K";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(56, 17);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "1080P";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(6, 65);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(149, 17);
            this.checkBox9.TabIndex = 4;
            this.checkBox9.Text = "Enable low Settings Mode";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.CheckBox9_CheckedChanged);
            // 
            // VIP_Checkbox
            // 
            this.VIP_Checkbox.AutoSize = true;
            this.VIP_Checkbox.Checked = global::TT2Bot.Properties.Settings.Default.VIP;
            this.VIP_Checkbox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::TT2Bot.Properties.Settings.Default, "VIP", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.VIP_Checkbox.Location = new System.Drawing.Point(6, 42);
            this.VIP_Checkbox.Name = "VIP_Checkbox";
            this.VIP_Checkbox.Size = new System.Drawing.Size(49, 17);
            this.VIP_Checkbox.TabIndex = 3;
            this.VIP_Checkbox.Text = "V.I.P";
            this.VIP_Checkbox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Screen Resolution:";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(179, 2);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 12;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(-3, 492);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(439, 116);
            this.richTextBox1.TabIndex = 5;
            this.richTextBox1.Text = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" +
    "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
            // 
            // timerFairy
            // 
            this.timerFairy.Tick += new System.EventHandler(this.TimerFairy_Tick);
            // 
            // timerPositionMana
            // 
            this.timerPositionMana.Tick += new System.EventHandler(this.TimerPosition_Tick);
            // 
            // timerPrestige
            // 
            this.timerPrestige.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // timerPJLevel
            // 
            this.timerPJLevel.Tick += new System.EventHandler(this.TimerPJLevel_Tick);
            // 
            // timerAttack
            // 
            this.timerAttack.Tick += new System.EventHandler(this.TimerAttack_Tick);
            // 
            // timerTime
            // 
            this.timerTime.Tick += new System.EventHandler(this.TimerTime_Tick);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.flowLayoutPanel1.Controls.Add(this.pictureBox1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(434, -5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(399, 613);
            this.flowLayoutPanel1.TabIndex = 16;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(399, 610);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // timerAbs
            // 
            this.timerAbs.Tick += new System.EventHandler(this.TimerAbs_Tick);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(260, 2);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 15;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.ButtonStop_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.Red;
            this.buttonClose.ForeColor = System.Drawing.Color.White;
            this.buttonClose.Location = new System.Drawing.Point(353, 2);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 17;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = false;
            this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 606);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TT2Bot";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox VIP_Checkbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.CheckBox checkBoxWarCry;
        public System.Windows.Forms.CheckBox checkBoxHandMidas;
        public System.Windows.Forms.CheckBox checkBoxDeadlyStrike;
        public System.Windows.Forms.Label labelRuntime;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.DomainUpDown ScreenShotTime;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.CheckBox checkBoxPrestige;
        public System.Windows.Forms.CheckBox checkBoxPJLev;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox textBoxPJLevel;
        public System.Windows.Forms.TextBox textBoxPrestige;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox textBoxAttack;
        public System.Windows.Forms.CheckBox checkBoxAttack;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox textBoxAbTime;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label labelDiamonds;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label labelStages;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label labelRelics;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label labelPrestiges;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label labelMoney;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label labelMana;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.Button buttonStart;
        public System.Windows.Forms.Timer timerWindowScreenShot;
        public System.Windows.Forms.Timer timerFairy;
        public System.Windows.Forms.Timer timerPositionMana;
        public System.Windows.Forms.Timer timerPrestige;
        public System.Windows.Forms.Timer timerPJLevel;
        public System.Windows.Forms.Timer timerAttack;
        public System.Windows.Forms.Timer timerTime;
        public System.Windows.Forms.Timer timerAbs;
        public System.Windows.Forms.Button buttonStop;
        public System.Windows.Forms.Button buttonClose;
    }
}


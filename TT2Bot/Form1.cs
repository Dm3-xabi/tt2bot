﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoItX3Lib;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using TT2Bot.Properties;

namespace TT2Bot
{
    public partial class Form1 : Form
    {
        public static AutoItX3 autoit = new AutoItX3();
        public static Form1 self;
        public bool ninetypercent, abDone = false;
        public bool foundFairy, stopClicked;
        public Process bs;
        public int diamonds = 0;
        public int prestiges = 0;
        public Bitmap img;

        //Different Classes
        BotMethods botMethods;
        ImageSearchMethods imageSearchMethods;


        public Form1()
        {
            InitializeComponent();
            timerWindowScreenShot.Interval = 400;
            timerPositionMana.Interval = 4000;
            timerPositionMana.Enabled = false;
            Init();
        }

        private void Init()
        {
            self = this;
            LoadSettings(Settings.Default);
            BotSession.start = DateTime.Now;
            timerTime.Interval = 1000;
            botMethods = new BotMethods();
        }
        

        private void LoadSettings(Settings @default)
        {
            if (Settings.Default.Is2K)
            {
                radioButton2.Checked = true;
            }
            else
            {
                radioButton1.Checked = true;
            }
        }


        private void ButtonStart_Click(object sender, EventArgs e)
        {
            BotSession.isBotRunning = true;
            timerTime.Start();
            autoit.WinActivate("NoxPlayer");

            //    //botMethods.CheckWindow();
            //    timerFairy.Interval = 6000;
            //    timerFairy.Start();


            if (checkBoxPJLev.Checked)
            {
                if (textBoxPJLevel.Text == null || textBoxPJLevel.Text == "")
                {
                    MessageBox.Show("You need to put a leveling time");
                }
                else
                {
                    int minutes = int.Parse(textBoxPJLevel.Text);
                    timerPJLevel.Interval = (int)TimeSpan.FromMinutes(minutes).TotalMilliseconds;
                    timerPJLevel.Start();
                }
            }
            if (checkBoxPrestige.Checked)
            {
                if (textBoxPrestige.Text == null || textBoxPrestige.Text == "")
                {
                    MessageBox.Show("You need to put a prestige time");
                }
                else
                {
                    int minutes2 = int.Parse(textBoxPrestige.Text);
                    timerPrestige.Interval = (int)TimeSpan.FromMinutes(minutes2).TotalMilliseconds;
                    timerPrestige.Start();
                }
            }
            if (checkBoxAttack.Checked)
            {
                if (textBoxAttack.Text == null || textBoxAttack.Text == "")
                {
                    MessageBox.Show("You need to put a Attack time");
                }
                else
                {
                    int secs = int.Parse(textBoxAttack.Text);
                    timerAttack.Interval = (int)TimeSpan.FromSeconds(secs).TotalMilliseconds;
                    timerAttack.Start();
                }
            }
            timerPositionMana.Start();
        }

        private void TimerFairy_Tick(object sender, EventArgs e)
        {
            imageSearchMethods.searchFairy();
            int screenHeight = Form1.self.Height;
            int screenWidth = Form1.self.Width;
            string tx = screenHeight + "," + screenWidth;
            //botMethods.WriteLine(tx);
        }
        



      
        //public void ImageDeleter(String dirName)
        //{
        //    string[] files = Directory.GetFiles(dirName);

        //    foreach (string file in files)
        //    {
        //        FileInfo fi = new FileInfo(file);
        //        if (fi.LastAccessTime < DateTime.Now.AddMilliseconds(-34000))
        //            fi.Delete();
        //    }
        //} 
        
        //Execute BlueStacks with game
       

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)    //para ver si es 2k o no
        {
            if (radioButton1.Checked)
            {
                Settings.Default.Is2K = false;  //guarda en la variable de settings
            }
            else
            {
                Settings.Default.Is2K = true;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            SaveSettings(Settings.Default); //guarda las settings
        }

        private void SaveSettings(Settings settings)    //para guardar los settings
        {
            settings.Save();
            //botMethods.WriteLine("Settings Fucked");

        }
       
        private void CheckBox9_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox9.Checked)
            {
                Settings.Default.LowSettings = true;  //guarda en la variable de settings
            }
            else
            {
                Settings.Default.LowSettings = false;  //guarda en la variable de settings
            }
        }
        private void TimerPosition_Tick(object sender, EventArgs e)
        {
            
            botMethods.posicionarBlue();
            int secs = int.Parse(textBoxAbTime.Text);
            timerAbs.Interval = (int)TimeSpan.FromSeconds(secs).TotalMilliseconds;
            timerAbs.Start();
        }        
        private void Timer_Tick(object sender, EventArgs e)
        {
            //autoit.Run(@"C:\Program Files (x86)\AutoIt3\AutoIt3.exe", @"D:\Project\tt2\TT2Bot\Scripts\1080p\Prestigio.au3", 1);
            //System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //proc.EnableRaisingEvents = false;
            //proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Prestigio.au3";
            //proc.Start();
            botMethods.MakePrestige();
            //botMethods.WriteLine("Prestige has been done!");
            prestiges += 1;
            labelPrestiges.Text = prestiges.ToString();
        }

        private void TimerPJLevel_Tick(object sender, EventArgs e)
        {
            timerAttack.Stop();
            botMethods.upgradeLevel();
            
            //autoit.MouseClick("left", 1028, 763, 1, -1);
            //autoit.Sleep(2000);
            //int zenb = 0;
            //while(zenb <= 3){ 
            //    //var contratar = autoit.PixelSearch(1186, 603, 1270, 752, 0xF7A605, 5); //encuentra color subir nivel
            //    //if (contratar != null) {
            //    //    autoit.MouseClick("left", contratar[0], contratar[1], 2, 1);
            //    //}
            //    var subirnivel = autoit.PixelSearch(1186, 603, 1270, 752, 0xEF8314, 5); //encuentra color subir nivel
            //    BotMethods.WriteLine(subirnivel.ToString());
            //    if (subirnivel != null || subirnivel.Length != 1 || subirnivel[0] != 0) {
            //        autoit.MouseClick("left", subirnivel[0], subirnivel[1], 2, 1);
            //    }
            //    Array.Clear(subirnivel, 0, subirnivel.Length);
            //    autoit.MouseClickDrag("left", 1087, 733, 1099, 584);
            //    zenb = zenb + 1;
            //}
            //autoit.MouseClickDrag("left", 1099, 584, 1087, 733);    //volver a su estado original
            //autoit.MouseClickDrag("left", 1099, 584, 1087, 733);
            //autoit.MouseClick("left", 1028, 763, 1, -1);
            //charAb();       //aumentar nivel personajes
        }

        

        private void TimerAttack_Tick(object sender, EventArgs e)
        {
            botMethods.autoClick();
        }

        private void TimerTime_Tick(object sender, EventArgs e)
        {
            TimeSpan runtimes = DateTime.Now - BotSession.start;
            labelRuntime.Text = $"{runtimes.Hours}:{runtimes.Minutes}:{runtimes.Seconds}";
            //ImageSearch img = new ImageSearch();
            //img.CaptureApplication("BlueStacks");
        }

        private void TimerAbs_Tick(object sender, EventArgs e)
        {
            bool encuentro = imageSearchMethods.manaDetector();
            if (encuentro == true)
            {
                abDone = imageSearchMethods.useAbilities();
                if (abDone == true)
                {
                    //botMethods.WriteLine("Chosen abilities used");
                }
            }
            else
            {
                //botMethods.WriteLine("There isn't any mana available");
            }
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            botMethods.StopBot();
            this.Close();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Alt && e.KeyCode == Keys.F4)
            {
                timerWindowScreenShot.Stop();
                timerAttack.Stop();
                timerFairy.Stop();
                timerPJLevel.Stop();
                timerPositionMana.Stop();
                timerPrestige.Stop();
                timerTime.Stop();
                try
                {
                    foreach (Process proc in Process.GetProcessesByName("AutoIt"))
                    {
                        proc.Kill();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                this.Close();
            }
            if (e.KeyCode == Keys.Control && e.KeyCode == Keys.S)
            {
                botMethods.StopBot();
            }
        }

        private void ButtonStop_Click(object sender, EventArgs e)
        {
            botMethods.StopBot();
            stopClicked = true;
        }
    }
}

    


#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

HotKeySet("{esc}","myExit")
WinActivate("BlueStacks")
Send("SHIFTUP")
Send("SHIFTDOWN")
myExit()

Func myExit()
   Exit
EndFunc

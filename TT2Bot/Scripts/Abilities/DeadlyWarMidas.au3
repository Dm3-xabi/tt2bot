#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

HotKeySet("{esc}","myExit")
WinActivate("BlueStacks")

Func myExit()
   Exit
EndFunc

$deadly = PixelSearch(1036, 703, 1082, 739, 0x194200, 0, 1)
If Not @error Then
   If UBound($deadly) >= 2 Then
	  MouseClick("LEFT", $deadly[0], $deadly[1], 1, -1)
   EndIf
EndIf
$war = PixelSearch(1183, 701, 1226, 739, 0xDC7610, 0, 1)
If Not @error Then
   If UBound($war) >= 2 Then
	  MouseClick("LEFT", $war[0], $war[1], 1, -1)
   EndIf
EndIf
$midas = autoit.PixelSearch(1088, 703, 1131, 739, 0x6D6BAA, 0, 1);
If Not @error Then
   If UBound($midas) >= 2 Then
	  autoit.MouseClick("LEFT", $midas[0], $midas[1], 1, -1)
   EndIf
EndIf
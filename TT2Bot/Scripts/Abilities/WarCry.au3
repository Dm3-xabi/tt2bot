#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

HotKeySet("{esc}","myExit")
WinActivate("BlueStacks")

Func myExit()
   Exit
EndFunc

$war = PixelSearch(1183, 701, 1226, 739, 0xDC7610, 0, 1)
If Not @error Then
   If UBound($war) >= 2 Then
	  MouseClick("LEFT", $war[0], $war[1], 1, -1)
   EndIf
EndIf
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

HotKeySet("{esc}","myExit")
WinActivate("BlueStacks")


$deadly = PixelSearch(1036, 703, 1082, 739, 0xEDF293, 5, 1)
If Not @error Then
   If UBound($deadly) >= 2 Then
	  MouseClick("LEFT", $deadly[0], $deadly[1], 1, -1)
   EndIf
EndIf


Func myExit()
   Exit
EndFunc
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

HotKeySet("{esc}","myExit")
WinActivate("BlueStacks")
while 1
   $verdeHada = PixelSearch(994, 360, 1268, 403, 0x689C2C,5)	;Hada vestido
   $recoger = PixelSearch(994, 360, 1268, 403, 0x28A1CB,5)	;Recoger
   If Not @error Then
	  hadas()
   EndIf
   myExit()
WEnd
Func hadas()
   If UBound($verdeHada) >= 2 Then
	  MouseClick("left", $verdeHada[0], $verdeHada[1],2,1)
   EndIf
   If UBound($recoger) >= 2 Then
	  MouseClick("left", $recoger[0], $recoger[1],2,1)
   EndIf
EndFunc
myExit()

Func myExit()
   Exit
EndFunc
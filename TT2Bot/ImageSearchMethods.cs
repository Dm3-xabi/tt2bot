﻿using AutoItX3Lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TT2Bot.Properties;

namespace TT2Bot
{
    class ImageSearchMethods
    {

        public bool ninetypercent, abDone = false;
        public static AutoItX3 autoit = new AutoItX3();
        public bool foundFairy;


        public BotMethods botMethods;

        public ImageSearchMethods()
        {
            botMethods = new BotMethods();
        }

        //Hace capturas
        public void readText(string imgSource)
        {
            


        }


        [DllImport(@"C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Scripts\ImageSearchDLL.dll")]
        public static extern IntPtr ImageSearch(int x, int y, int right, int bottom, [MarshalAs(UnmanagedType.LPStr)]string imagePath);

        //public static string[] UseImageSearch(string imgPath, string tolerance)
        //{
        //    imgPath = "*" + tolerance + " " + imgPath;

        //    IntPtr result = ImageSearch(0, 0, 2560,1440, imgPath);
        //    string res = Marshal.PtrToStringAnsi(result);

        //    if (res[0] == '0') return null;

        //    string[] data = res.Split('|');

        //    int x; int y;
        //    int.TryParse(data[1], out x);
        //    int.TryParse(data[2], out y);

        //    return data;
        //}


        public static string[] UseImageSearch(string imgPath)
        {
            int right = Screen.PrimaryScreen.WorkingArea.Right;
            int bottom = Screen.PrimaryScreen.WorkingArea.Bottom;

            IntPtr result = ImageSearch(0, 0, right, bottom, imgPath);
            string res = Marshal.PtrToStringAnsi(result);

            if (res[0] == '0') return null;//not found

            string[] data = res.Split('|');
            //0->found, 1->x, 2->y, 3->image width, 4->image height;        

            // Then, you can parse it to get x and y:
            int x; int y;
            int.TryParse(data[1], out x);
            int.TryParse(data[2], out y);

            return data;
        }
        public void searchImage(string direction)
        {
            string image = (direction);

            string[] results = UseImageSearch(image);
            if (results == null)
            {
                MessageBox.Show("null value bro, sad day");
            }
            else
            {
                MessageBox.Show(results[1] + ", " + results[2]);
            }
        }

        public bool scan(string image)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Bitmap screenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            Graphics g = Graphics.FromImage(screenCapture);

            g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
                             Screen.PrimaryScreen.Bounds.Y,
                             0, 0,
                             screenCapture.Size,
                             CopyPixelOperation.SourceCopy);

            Bitmap myPic = new Bitmap(image);

            bool isInCapture = IsInCapture(myPic, screenCapture);

            sw.Stop();

            TimeSpan ts = sw.Elapsed;
            return isInCapture;
        }

        public bool IsInCapture(Bitmap searchFor, Bitmap searchIn)
        {
            for (int x = 0; x < searchIn.Width; x++)
            {
                for (int y = 0; y < searchIn.Height; y++)
                {
                    bool invalid = false;
                    int k = x, l = y;
                    for (int a = 0; a < searchFor.Width; a++)
                    {
                        l = y;
                        for (int b = 0; b < searchFor.Height; b++)
                        {
                            if (searchFor.GetPixel(a, b) != searchIn.GetPixel(k, l))
                            {
                                invalid = true;
                                break;
                            }
                            else
                                l++;
                        }
                        if (invalid)
                            break;
                        else
                            k++;
                    }
                    if (!invalid)
                        return true;
                }
            }
            return false;
        }


        public void ImageComparator()
        {

            //string img = @"C:\Users\Xabi\Desktop\Captura.tiff";
            ////var Ocr = new AutoOcr();
            ////var Result = Ocr.Read(img);
            ////labelMoney.Text = Result.Text;
            ////img2 = imga.captureArea(pictureBox1);
            //TesseractEngine engine = new TesseractEngine("./tessdata", "eng", EngineMode.Default);
            //var imga = Pix.LoadFromFile(img);
            //var page = engine.Process(imga);

            //////Page page = engine.Process(img, PageSegMode.Auto);
            //////Page page2 = engine.Process(img2, PageSegMode.Auto);
            //labelMoney.Text = page.GetText();
            ////labelMana.Text = page.GetText();
        }
        String capsDir = @"C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Caps\";
        public void ImageController()
        {

            /// SCANNING ALL ACTIVE SKILLS IMAGE
            //bool itis = img.scan(@"C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Fonts\AllActiveSkills.JPG");
            //if (itis == true)
            //{
            //    //timerWindowScreenShot.Stop();
            //    //System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //    //proc.EnableRaisingEvents = false;
            //    //proc.StartInfo.FileName = @"C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Scripts\RecogerHadas.au3";
            //    //proc.Start();
            //    MessageBox.Show("Encuentra");

            //}
            bool itis2 = scan(@"C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Fonts\HadaDinero.JPG");
            if (itis2 == true)
            {
                //timerWindowScreenShot.Stop();
                //System.Diagnostics.Process proc = new System.Diagnostics.Process();
                //proc.EnableRaisingEvents = false;
                //proc.StartInfo.FileName = @"C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Scripts\RecogerHadas.au3";
                //proc.Start();
                MessageBox.Show("Encuentra");

            }
            else
            {
                MessageBox.Show("No encuentra");
            }
            //string fairy = ImageSearch.Search("BlueStacks", @"D:\Proiekt\TT2\TT2Bot\Fonts\Hada.PNG");
            ////if (fairy != null)
            //{
            //    timerWindowScreenShot.Stop();
            //    System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //    proc.EnableRaisingEvents = false;
            //    proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Hadas.au3";
            //    proc.Start();

            //}
            ////detectar imagenes
            ////string moneyNineTeen = ImageSearch.Search("BlueStacks", @"D:\Proiekt\TT2\TT2Bot\Fonts\Fairy90.JPG");
            ////if (moneyNineTeen != null)
            //{
            //    BotMethods.WriteLine("90% money activated");
            //    timerWindowScreenShot.Stop();
            //    ninetypercent = true;       //TODO : algo que detecte que se ha terminado el bufo de 1 min
            //    System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //    proc.EnableRaisingEvents = false;
            //    proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\RecogerHadas.au3";
            //    proc.Start();

            //}
            ////string diamonds = ImageSearch.Search("BlueStacks", @"D:\Proiekt\TT2\TT2Bot\Fonts\FairyDiamonds.JPG");
            //if (diamonds != null)       //diamonds nunca es null
            //{
            //    diamonds = diamonds + 10;
            //    labelDiamonds.Text = diamonds.ToString();
            //    BotMethods.WriteLine("10 Diamonds Collected");
            //    timerWindowScreenShot.Stop();
            //    System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //    proc.EnableRaisingEvents = false;
            //    proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\RecogerHadas.au3";
            //    proc.Start();
            //}
            ////string skills = ImageSearch.Search("BlueStacks", @"D:\Proiekt\TT2\TT2Bot\Fonts\AllActiveSkills.JPG");
            ////if (skills != null)
            //{
            //    BotMethods.WriteLine("All Skills activated");
            //    timerWindowScreenShot.Stop();
            //    System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //    proc.EnableRaisingEvents = false;
            //    proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\RecogerHadas.au3";
            //    proc.Start();
            //}
            ////string money = ImageSearch.Search("BlueStacks", @"D:\Proiekt\TT2\TT2Bot\Fonts\FairyMoney-Clean.PNG");
            ////if (money != null)
            //{
            //    //diamonds = diamonds + 10;
            //    //labelDiamonds.Text = diamonds.ToString();
            //    //BotMethods.WriteLine("10 Diamonds Collected");
            //    timerWindowScreenShot.Stop();   //TODO :  algo que detecte cantidad
            //    System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //    proc.EnableRaisingEvents = false;
            //    proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\RecogerHadas.au3";
            //    proc.Start();
            //}
            //timerWindowScreenShot.Start();
        }
        public bool useAbilities()
        {
            abDone = false;
            if (Form1.self.checkBoxDeadlyStrike.Checked == true && Form1.self.checkBoxWarCry.Checked == true)
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Abilities\DeadlyWar.au3";
                proc.Start();
                abDone = true;
            }
            if (Form1.self.checkBoxWarCry.Checked == true)
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Abilities\Warcry.au3";
                proc.Start();
                abDone = true;
            }
            if (Form1.self.checkBoxDeadlyStrike.Checked == true)
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Abilities\Deadly.au3";
                proc.Start();
                abDone = true;
            }
            if (Form1.self.checkBoxDeadlyStrike.Checked == true && Form1.self.checkBoxWarCry.Checked == true && Form1.self.checkBoxHandMidas.Checked == true)
            {
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Abilities\DeadlyWarMidas.au3";
                proc.Start();
                abDone = true;
            }


            return abDone;
        }

        public bool manaDetector()
        {
            Rectangle rect = new Rectangle(1126, 671, 1137, 684);
            Rectangle rect2 = new Rectangle(1055, 674, 1064, 682);

            var autoSumPoint = autoit.PixelSearch(1099, 673, 1124, 683, 0xFFFFFF);
            var autoSumPoint2 = autoit.PixelSearch(1055, 674, 1064, 682, 684, 0xFFFFFF);

            if (autoSumPoint == null || autoSumPoint2 == null)
            {
                return false;
            }
            else
            {
                //botMethods.WriteLine("Mana found");
                return true;
            }
        }
        private void charAb()
        {
            autoit.MouseClick("left", 1055, 767, 1, -1);
            autoit.Sleep(2000);
            int zenb = 0;
            while (zenb <= 3)
            {
                var contratar = autoit.PixelSearch(1186, 603, 1270, 752, 0xF7A605, 5); //encuentra color subir nivel
                if (contratar != null)
                {
                    autoit.MouseClick("left", contratar[0], contratar[1], 2, 1);
                }
                var subirnivel = autoit.PixelSearch(1186, 603, 1270, 752, 0xEF8314, 5); //encuentra color subir nivel
                if (subirnivel != null)
                {
                    autoit.MouseClick("left", subirnivel[0], subirnivel[1], 2, 1);
                }
                autoit.MouseClickDrag("left", 1087, 733, 1099, 584);
                zenb = zenb + 1;
            }
            autoit.MouseClickDrag("left", 1099, 584, 1087, 733);
            autoit.MouseClickDrag("left", 1099, 584, 1087, 733);
            autoit.MouseClick("left", 1028, 763, 1, -1);
            Form1.self.timerAttack.Start();
        }

        public void searchFairy()
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.EnableRaisingEvents = false;
            proc.StartInfo.FileName = @"D:\Proiekt\TT2\TT2Bot\Scripts\Hadas.au3";
            proc.Start();
            foundFairy = true;
            ImageController();
        }
    }
}


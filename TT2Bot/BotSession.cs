﻿using AutoItX3Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT2Bot
{
   public static class BotSession
    {
        public static AutoItX3 autoit = new AutoItX3();
        public static bool isBotRunning { get; set; }
        public static Values_Stub resolution_values { get; set; }
        public static DateTime start;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT2Bot
{
    class BotSettings
    {
        public int Resolution { get; set; }
        public bool VIP { get; set; }
        public bool LowSettingsMode { get; set; }

    }
}

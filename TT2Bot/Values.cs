﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT2Bot
{
    public class Values_Stub
    {
        private int[] recoger;
        private int[] searingLight;
        private int[] cordsW;
        private int[] cordsE;
        private int[] cordsT;
        private int[] fightBoss;
        private int[] verdeHada;

        public int[] SearingLight { get => searingLight; set => searingLight = value; }
        public int[] CordsW { get => cordsW; set => cordsW = value; }
        public int[] CordsE { get => cordsE; set => cordsE = value; }
        public int[] CordsT { get => cordsT; set => cordsT = value; }
        public int[] FightBoss { get => fightBoss; set => fightBoss = value; }
        public int[] VerdeHada { get => verdeHada; set => verdeHada = value; }
        public int[] Recoger { get => recoger; set => recoger = value; }

        public Values_Stub(int[] searingLight, int[] cordsW, int[] cordsE, int[] cordsT, int[] fightBoss, int[] verdeHada, int[] recoger)
        {
            this.searingLight = searingLight;
            this.cordsW = cordsW;
            this.cordsE = cordsE;
            this.cordsT = cordsT;
            this.fightBoss = fightBoss;
            this.verdeHada = verdeHada;
            this.recoger = recoger;
        }

        public Values_Stub()
        {
        }

    }
}

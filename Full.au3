#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

;GUI

#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <Timers.au3>
#Region ### START Koda GUI section ### Form=C:\Users\Xabi\source\repos\tt2bot\TT2Bot\Form1.kxf
$Form1 = GUICreate("Form1", 322, 236, -1, -1)
$labelPrestige = GUICtrlCreateLabel("Enter prestige time:", 24, 48, 94, 17)
$InputPrestige = GUICtrlCreateInput("", 160, 48, 121, 21)
$CheckboxUpgradePj = GUICtrlCreateCheckbox("Auto update pj and ch", 24, 80, 129, 17)
$CheckboxFairies = GUICtrlCreateCheckbox("Take fairies", 24, 112, 97, 17)
$ButtonStart = GUICtrlCreateButton("Start", 64, 168, 75, 25)
$ButtonStop = GUICtrlCreateButton("Stop", 216, 168, 75, 25)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
Local $upgradePJCH = False
Local $fairies = False
Global $prestigeTime

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		 Case $ButtonStart
			$startTime = TimerInit()
			$prestigeTime = GUICtrlRead($InputPrestige)
			If GUICtrlRead($CheckboxUpgradePJ) = 1 Then $upgradePJCH = true
			If GUICtrlRead($CheckboxFairies) = 1 Then $fairies = true
			Start($prestigeTime, $upgradePJCH, $fairies)
			HotKeySet("{esc}","myExit")
			WinActivate("BlueStacks")
		 Case $ButtonStop
			$finishTime = TimerDiff($startTime)
			$runingTime = $finishTime / 60000
			$extract = StringMid($runingTime, 1, StringInStr($runingTime, ".", 1, 1) -1)
			MsgBox(0,"Time","The bot has been running: " &$extract & "minutes")
			GUIDelete($Form1)
            Exit
	EndSwitch
 WEnd


Func Start($prestigeTime, $upgradePJCH, $fairies)
   ;prestige timer

   ;fairy checker and clicker
   If $fairies = 1 Then Fairies()

   ;AntiLeave battle

   ; Auto Click

   ;Auto abilities
EndFunc
Func TimedActivated()
    Dim $myPrestigeTimer
    _Timer_KillTimer($hGUI, $myPrestigeTimer)
    msgMe()
 EndFunc
Func msgMe()
 MsgBox(0,'','Finished')
EndFunc

Func Fairies()
   while 1
	  $verdeHada = PixelSearch(2134, 529, 2321, 589, 0x689C2C,5)	;Hada vestido
	  $recoger = PixelSearch(2134, 529, 2321, 589, 0x28A1CB,5)	;Recoger
	  If Not @error Then
		 If UBound($verdeHada) >= 2 Then
		 MouseClick("left", $verdeHada[0], $verdeHada[1],2,1)
		 EndIf
		 If UBound($recoger) >= 2 Then
			MouseClick("left", $recoger[0], $recoger[1],2,1)
		 EndIf
	  EndIf
   WEnd
EndFunc


Func myExit()
   Exit
EndFunc

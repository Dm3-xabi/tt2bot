#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=
Global $Form1 = GUICreate("Form1", 216, 85, 192, 124)
Global $Button1 = GUICtrlCreateButton("Start", 16, 24, 75, 25)
Global $Button2 = GUICtrlCreateButton("Stop", 120, 24, 75, 25)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
	   Case $Button2
		  Exit
	   Case $Button1
		  Start()
	   Case $GUI_EVENT_CLOSE
		  Exit
	EndSwitch
WEnd

HotKeySet("{esc}","myExit")
WinActivate("BlueStacks")
;WinMove("BlueStacks","",0,0)
Global $cords, $cordsW, $cordsE, $cordsT, $fightBoss, $verdeHada
Func Start()
   While(1)
	  $cords = PixelSearch(2266, 344, 2312, 394, 0x899BA2,5)	;searingLight
	  If Not @error Then
		 searingLight()
	  EndIf
	  $cordsW = PixelSearch(2222, 659, 2350, 709, 0xAABE19,5)	;W
	  $cordsE = PixelSearch(2293, 663, 2346, 705, 0x7171B3,5)	;E
	  $cordsT = PixelSearch(2426, 656, 2477, 704, 0xFCE355,5)	;T
	  If Not @error Then
		 habilidades()
	  EndIf
	  $fightBoss = PixelSearch(2451, 58, 2549, 99, 0xEF6310,5)	;Fight Boss
	  If Not @error Then
		 fightBoss()
	  EndIf
	  $peloHada = PixelSearch(2153, 180, 2539, 259, 0xFFBF21,5)	;Hada vestido
	  $verdeHada = PixelSearch(2153, 180, 2539, 259, 0x54A924,5)	;Hada vestido
	  $peloHada2 = PixelSearch(2153, 180, 2539, 259, 0xD4F1F7,5)	;Recoger
	  $alasHada = PixelSearch(2153, 180, 2539, 259, 0xED9E2E,5)	;Recoger

	  $recoger = PixelSearch(2369, 550, 2518, 595, 0x28A9D3,5)	;Recoger
	  If Not @error Then
		 hadas()
	  EndIf
   WEnd
EndFunc

Func myExit()
   Exit
EndFunc
Func habilidades()
   If UBound($cordsW) >= 2 Then
	  MouseClick("left",$cordsW[0],$cordsW[1],2,1)
	  Sleep(250)
   EndIf
   If UBound($cordsE) >= 2 Then
	  MouseClick("left", $cordsE[0], $cordsE[1],2,1)
	  Sleep(250)
   EndIf
   If UBound($cordsT) >= 2 Then
	  MouseClick("left", $cordsT[0], $cordsT[1],2,1)
   EndIf
   ;Sleep(84000)
EndFunc
Func searingLight()
   If UBound($cords) >= 2 Then
	  MouseClick("left", $cords[0], $cords[1],2,1)
   EndIf
   ;Sleep(6000)
EndFunc
Func fightBoss()
   If UBound($fightBoss) >= 2 Then
	  MouseClick("left", $fightBoss[0], $fightBoss[1],2,1)
   EndIf

   ;Sleep(6000)
EndFunc
Func hadas()
   If UBound($verdeHada) >= 2 Then
	  MouseClick("left", $verdeHada[0], $verdeHada[1],2,1)
   EndIf

   If UBound($peloHada) >= 2 Then
	  MouseClick("left", $peloHada[0], $peloHada[1],2,1)
   EndIf
   If UBound($peloHada2) >= 2 Then
	  MouseClick("left", $peloHada2[0], $peloHada2[1],2,1)
   EndIf
   If UBound($alasHada) >= 2 Then
	  MouseClick("left", $alasHada[0], $alasHada[1],2,1)
   EndIf
   If UBound($recoger) >= 2 Then
	  MouseClick("left", $recoger[0], $recoger[1],2,1)
   EndIf

   ;Sleep(6000)
EndFunc
Func hacerPrestigio()
   Local $textval= ControlGetText("BlueStacks","","[CLASS:BlueStacksApp; INSTANCE:1]")

EndFunc

